<?php

namespace App\Http\Controllers\Planner;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\Planner;
use App\Monteur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class MonteurController extends Controller
{
    /** This function will not showing the page without login */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monteurs = Monteur::all();
        return view('planner.monteurs.index')->with('monteurs', $monteurs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Monteur $monteur)
    {
        $roles = Role::all();

        return view('planner.monteurs.create')->with([
            'monteur' => $monteur,
            'roles' => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'function' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => [
                'required', 'min:8', 'regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/' , 'confirmed'
            ],
        ]);

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'role_id' => 3,
        ]);
        $monteur = Monteur::create([
            'name' => $request->input('name'),
            'function' => $request->input('function'),
            'email' => $request->input('email'),
            'user_id' => $user->id,
            'appointment_id' => 0
        ]);

        return redirect()->route('planner.monteurs.index')->with('success', ''.$monteur->name.' is toegevoegd');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Monteur $monteur)
    {

        return view('planner.monteurs.edit')->with([
            'monteur' => $monteur,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Monteur $monteur)
    {
        $user = User::find($monteur->user_id);

        $this->validate($request, [
            'name' => 'required|max:255',
            'function' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
        ]);

        $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'role_id' => 3,
        ]);

        $monteur->name = $request->input('name');
        $monteur->email = $request->input('email');
        $monteur->function = $request->input('function');
        $monteur->save();

        return redirect()->route('planner.monteurs.index')->with('success', ''.$monteur->name.' is bijgewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Monteur $monteur)
    {
        $user = User::find($monteur->user_id);
//        $monteur->roles()->detach();
        $user->delete();
        $monteur->delete();

        return redirect()->route('planner.monteurs.index')->with('success', ''.$monteur->name.' is verwijderd');
    }
}
