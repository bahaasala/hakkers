<?php

namespace App\Http\Controllers\Planner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    /** This function will not showing the page without login */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        return view('planner.customers.index')->with('customers', $customers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Customer $customer)
    {
        $customers = Customer::all();
        return view('planner.customers.create')->with('customer', $customer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:customers,name',
            'address' => 'required',
            'zipcode' => 'required',
            'city' => 'required',
            'phone' => 'required|min:10',
        ]);

        $customer = Customer::create([
            'name' => $request->input('name'),
            'address' => $request->input('address'),
            'zipcode' => $request->input('zipcode'),
            'city' => $request->input('city'),
            'phone' => $request->input('phone'),
        ]);

        return redirect()->route('planner.customers.index')->with('success', ''.$customer->name.' is toegevoegd');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {

        return view('planner.customers.edit')->with('customer', $customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:customers,name,'.$customer->id,
            'address' => 'required',
            'zipcode' => 'required',
            'city' => 'required',
            'phone' => 'required|min:10',
        ]);

        $customer->name = $request->input('name');
        $customer->address = $request->input('address');
        $customer->zipcode = $request->input('zipcode');
        $customer->city = $request->input('city');
        $customer->phone = $request->input('phone');
        $customer->save();

        return redirect()->route('planner.customers.index')->with('success', ''.$customer->name.' is bijgewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();

        return redirect()->route('planner.customers.index')->with('success', ''.$customer->name.' is verwijderd');
    }
}
