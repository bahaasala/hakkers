<?php

namespace App\Http\Controllers\Planner;

use App\Appointment;
use App\Customer;
use App\Monteur;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    /** This function will not showing the page without login */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointments = Appointment::all();
        $appointments = Appointment::orderBy('created_at', 'desc')->get();
        $monteurs = Monteur::all();
        $customers = Customer::all();

        return view('planner.appointments.index')->with([
            'appointments' => $appointments,
            'monteurs' => $monteurs,
            'customers' => $customers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Appointment $appointment)
    {
        $appointments = Appointment::all();
        $monteurs = Monteur::all();
        $customers = Customer::all();

        return view('planner.appointments.create')->with([
            'appointment' => $appointment,
            'monteurs' => $monteurs,
            'customers' => $customers
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => 'required|after:now'
        ]);

        $time = Carbon::now();
        $now = $time->toDateTimeString();
        $appointment = Appointment::create([
            'customer_id' => $request->input('customer'),
            'monteur_id' => $request->input('monteur'),
            'date' => $request->input('date'),
            'type' => $request->input('type'),
            'note' => $request->input('note'),
            'depart_date' => $now,
            'arrived_date' => $now,
            'finished_date' => $now,
        ]);

        return redirect()->route('planner.appointments.index')->with('success', 'Afspraak is ingepland');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        $appointments = Appointment::all();
        $monteurs = Monteur::all();
        $customers = Customer::all();

        $timestamp= $appointment->date;
        $splitTimeStamp = explode(" ",$timestamp);
        $date = $splitTimeStamp[0];
        $time = $splitTimeStamp[1];


        return view('planner.appointments.edit')->with([
            'appointment' => $appointment,
            'monteurs' => $monteurs,
            'customers' => $customers,
            'date' => $date,
            'time' => $time,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        $this->validate($request, [
            'date' => 'required|after:now'
        ]);

        $appointment->customer_id = $request->input('customer');
        $appointment->monteur_id =$request->input('monteur');
        $appointment->date = $request->input('date');
        $appointment->type = $request->input('type');
        $appointment->note = $request->input('note');
        $appointment->save();

        return redirect()->route('planner.appointments.index')->with('success', 'Afspraak is bijgewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        $appointment->delete();

        return redirect()->route('planner.appointments.index')->with('success', 'Afspraak is verwijderd');
    }

    /** This function is to show the appointments */
    public function show(Appointment $appointment)
    {
        /* This variables calculate the working hours of the appointment  */
        $arrived = Carbon::parse($appointment->arrived_date);
        $finished = Carbon::parse($appointment->finished_date);
        $working_hours = $arrived->diffInSeconds($finished);
        $working_hours = gmdate('H,i', $working_hours);
        $working_hours = ltrim($working_hours, '0');

        return view('planner.appointments.single')->with([
            'appointment' => $appointment,
            'working_hours' => $working_hours
        ]);
    }
}
