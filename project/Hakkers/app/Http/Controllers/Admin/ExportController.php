<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Exports\AppointmentsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExportController extends Controller
{
    public function index()
    {
        return view('admin.data.index');
    }
    public function export()
    {
        return Excel::download(new AppointmentsExport, 'afspraken.xlsx');
    }
}
