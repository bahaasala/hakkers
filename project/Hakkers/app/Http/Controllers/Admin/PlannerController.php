<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\Planner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PlannerController extends Controller
{
    /** This function will not showing the page without login */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planners = Planner::all();

        return view('admin.planners.index')->with('planners', $planners);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Planner $planner)
    {
        $roles = Role::all();

        return view('admin.planners.create')->with([
            'planner' => $planner,
            'roles' => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => [
                'required', 'min:8', 'regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/' , 'confirmed'
            ],
        ]);


        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'role_id' => 2,
        ]);
        $planner = Planner::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'user_id' => $user->id,
        ]);

        return redirect()->route('admin.planners.index')->with('success', ''.$planner->name.' is toegevoegd');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Planner $planner)
    {
        $roles = Role::all();

        return view('admin.planners.edit')->with([
            'planner' => $planner,
            'roles' => $roles,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Planner $planner)
    {
        $user = User::find($planner->user_id);

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$user->id,
        ]);

        $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'role_id' => 2,
        ]);

        $planner->name = $request->input('name');
        $planner->email = $request->input('email');
        $planner->save();

        return redirect()->route('admin.planners.index')->with('success', ''.$planner->name.' is bijgewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Planner $planner)
    {
        $user = User::find($planner->user_id);
//        $planner->roles()->detach();
        $user->delete();
        $planner->delete();

        return redirect()->route('admin.planners.index')->with('success', ''.$planner->name.' is verwijderd');
    }
}
