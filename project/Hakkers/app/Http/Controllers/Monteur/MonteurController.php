<?php

namespace App\Http\Controllers\Monteur;

use App\Appointment;
use App\Customer;
use App\Http\Controllers\Controller;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;
use App\Monteur;
use Carbon\Carbon;

class MonteurController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $customers = Customer::all();
        $monteur = Monteur::orderBy('id', 'desc')->first();
        $appointments = $monteur->Appointments;
        $appointments = Appointment::orderBy('created_at', 'desc')->get();
        $today = Carbon::today()->toDateString();
        $tomorrow = Carbon::tomorrow()->toDateString();


        return view('monteur.appointments.index')->with([
            'appointments' => $appointments,
            'customers' => $customers,
            'monteur' => $monteur,
            'today' => $today,
            'tomorrow' => $tomorrow
        ]);
    }

    /** This function is to show the appointments */
    public function show(Appointment $appointment)
    {
        $customers = Customer::all();
        $monteur = Monteur::orderBy('id', 'desc')->first()->id;

        /* This variables calculate the working hours of the appointment  */
        $arrived = Carbon::parse($appointment->arrived_date);
        $finished = Carbon::parse($appointment->finished_date);
        $working_hours = $arrived->diffInSeconds($finished);
        $working_hours = gmdate('H,i', $working_hours);
        $working_hours = ltrim($working_hours, '0');

        return view('monteur.appointments.single')->with([
            'appointment' => $appointment,
            'customers' => $customers,
            'monteur' => $monteur,
            'working_hours' => $working_hours
        ]);
    }

    /** This function is to send the user to the options page */
    public function start($id)
    {
        $appointment = Appointment::where('id', $id)->first();

        return view('monteur.appointments.start')->with([
            'appointment' => $appointment,
        ]);
    }

    /** This function is to saving the date when the worker depart */
    public function departure($id)
    {

        $appointment = Appointment::where('id', $id)->first();
        $time = Carbon::now();
        $now = $time->toDateTimeString();

        $appointment->depart_date = $now;
        $appointment->save();

        return redirect()->to('/monteur/appointments/' . $appointment->id . '/start')->with('success','Monteur is vertokken');
    }

    /** This function is to saving the date when the worker arrived */
    public function arrive($id)
    {
        $appointment = Appointment::where('id', $id)->first();
        $mytime = Carbon::now();
        $now = $mytime->toDateTimeString();

        $appointment->arrived_date = $now;
        $appointment->save();

        return redirect()->to('/monteur/appointments/' . $appointment->id . '/start')->with('success','Monteur is Aangekomen');
    }

    /** This function is to saving the date when the worker finished */
    public function finish($id)
    {
        $appointment = Appointment::where('id', $id)->first();
        $mytime = Carbon::now();
        $now = $mytime->toDateTimeString();

        $appointment->finished_date = $now;
        $appointment->successful = '1';
        $appointment->save();

        return redirect()->to('/monteur/appointments/')->with('success','Monteur is Klaar');
    }
}
