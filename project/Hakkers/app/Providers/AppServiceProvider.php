<?php

namespace App\Providers;

use App\Role;
use Illuminate\Contracts\View\View;
use Illuminate\Routing\Route;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function(View $view){
            $roles = Role::all();

            $adminRole = Role::where('name', 'admin')->first();
            $plannerRole = Role::where('name', 'planner')->first();
            $monteurRole = Role::where('name', 'monteur')->first();

            $view->with('adminRole', $adminRole);
            $view->with('plannerRole', $plannerRole);
            $view->with('monteurRole', $monteurRole);
            $view->with('roles', $roles);

        });
    }
}
