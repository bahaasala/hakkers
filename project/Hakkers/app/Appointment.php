<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = [
        'customer_id', 'monteur_id', 'date', 'type', 'note', 'depart_date', 'arrived_date', 'finished_date'
    ];
    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }
    public function monteur()
    {
        return $this->belongsTo('App\Monteur', 'monteur_id');
    }
}
