<?php

namespace App\Exports;

use App\Appointment;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AppointmentsExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Appointment::all();
    }
    public function map($appointment): array
    {
        /* This variables calculate the working hours of the appointment  */
        $arrived = Carbon::parse($appointment->arrived_date);
        $finished = Carbon::parse($appointment->finished_date);
        $working_hours = $arrived->diffInSeconds($finished);
        $working_hours = gmdate('H,i', $working_hours);
        $working_hours = ltrim($working_hours, '0');


        if ($appointment->successful == 1){
            $successful = 'monteur is geweest';
        }else{
            $successful = 'afspraak staat nog open';
        }
        return [
            $appointment->customer->name,
            $appointment->monteur->name,
            $appointment->date,
            $appointment->type,
            $appointment->note,
            $appointment->depart_date,
            $appointment->arrived_date,
            $appointment->finished_date,
            $successful,
            $working_hours
        ];
    }
    public function headings(): array
    {
        return [
            'Klant',
            'Monteur',
            'Datum afspraak',
            'Soort melding',
            'Notitie',
            'Vertrokken',
            'Aangekomen',
            'Klaar',
            'Status',
            'Gewerkte uren'
        ];
    }
}
