<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monteur extends Model
{
    protected $fillable = [
        'name', 'function', 'email', 'user_id', 'appointment_id'
    ];
    public function Appointments() {
        return $this->hasMany(Appointment::class, "monteur_id", "id");
    }
}
