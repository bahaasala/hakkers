<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $adminRole = Role::where('name', 'admin')->first();
        $plannerRole = Role::where('name', 'planner')->first();
        $monteurRole = Role::where('name', 'monteur')->first();

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('adminadmin'),
            'role_id' => 1,
        ]);

        $planner = User::create([
            'name' => 'Planner',
            'email' => 'planner@planner.com',
            'password' => Hash::make('planner1'),
            'role_id' => 2,
        ]);

        $monteur = User::create([
            'name' => 'Monteur',
            'email' => 'monteur@monteur.com',
            'password' => Hash::make('monteur1'),
            'role_id' => 3,
        ]);

    }
}
