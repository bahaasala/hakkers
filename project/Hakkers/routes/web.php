<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function(){
    Route::resource('/planners', 'PlannerController');
    Route::resource('/monteurs', 'MonteurController');
    Route::resource('/users', 'UsersController');
    Route::get('/data', 'ExportController@index');
    Route::get('/data/download', 'ExportController@export');
});
Route::namespace('Planner')->prefix('planner')->name('planner.')->group(function(){
    Route::resource('/monteurs', 'MonteurController');
    Route::resource('/customers', 'CustomerController');
    Route::resource('/appointments', 'AppointmentController');
    Route::get('/appointments/{id}', 'AppointmentController@show');
});
Route::namespace('Monteur')->prefix('monteur')->name('monteur.')->group(function(){
    Route::resource('/appointments', 'MonteurController');
    Route::get('/appointments/{id}', 'MonteurController@show');
    Route::get('/appointments/{id}/start', 'MonteurController@start')->name('start.appointment');
        Route::get('/appointments/{id}/departure', 'MonteurController@departure')->name('departure.appointment');
        Route::get('/appointments/{id}/arrive', 'MonteurController@arrive')->name('arrive.appointment');
        Route::get('/appointments/{id}/finish', 'MonteurController@finish')->name('finish.appointment');
});
