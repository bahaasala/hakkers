// This function makes the rows clickable
$(function () {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});

// This function is for alert replay
$(function () {
    var count = 1;
    var audio = document.getElementById("play_audio");
    audio.onended = function() {
        if(count <= 1){
            count++;
            this.play();
        }
    };
});

/* Dynamic select box */