@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="spacer"></div>
            <div class="col-md-12">
                <h1>Welkom bij het meldingsysteem van Hakkers</h1>
            </div>
            <div class="spacer"></div>
            <div class="col-md-4">
                <p>
                    Lorem ipsum spiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                    totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae
                </p>
            </div>
            <div class="row">
                <img src="/images/home-page-workers1.png" class="home-img">
            </div>
        </div>
    </div>
</div>
@endsection
