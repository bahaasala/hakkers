@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Klanten</div>

                    <div class="card-body">
                        <a href="{{ route('planner.customers.create') }}"><button type="button" class="btn btn-success btn-add">Klant toevoegen</button></a>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Naam</th>
                                <th scope="col">Adres</th>
                                <th scope="col">Tel</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($customers as $customer)
                                    <tr>
                                        <td>{{ $customer->name }}</td>
                                        <td>{{  $customer->address }}<br> {{ $customer->zipcode }} {{ $customer->city }}</td>
                                        <td>{{  $customer->phone }}</td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <form action="{{ route('planner.customers.destroy', $customer) }}" method="POST" class="float-right">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger delete-warning">Verwijderen</button>
                                            </form>
                                            <a href="{{ route('planner.customers.edit', $customer->id) }}"><button type="button" class="btn btn-primary float-right btn-edit">Bewerken</button></a>
                                        </td>
                                    </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
