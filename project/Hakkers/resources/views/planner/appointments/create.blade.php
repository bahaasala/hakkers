@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Afspraak inplannen</div>
                    <div class="card-body">
                        <form action="{{  route('planner.appointments.store', $appointment) }}" method="POST">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Klant') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control @error('customer') is-invalid @enderror" name="customer" data-live-search="true" required>
                                        <option value="">-- Selecteer een klant --</option>
                                        @foreach($customers as $customer)
                                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('customer')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Monteur') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('monteur') is-invalid @enderror" name="monteur" data-live-search="true" required>
                                        <option value="">-- Selecteer een monteur --</option>
                                        @foreach($monteurs as $monteur)
                                            <option value="{{ $monteur->id }}">{{ $monteur->name }}</option>
                                        @endforeach
                                    </select>

                                    @error('monteur')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Datum en Tijd') }}</label>
                                <div class="col-md-6">
                                    <input type="datetime-local" id="" class="form-control @error('date') is-invalid @enderror" name="date">

                                    @error('date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Soort melding') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('type') is-invalid @enderror" name="type" data-live-search="true" required>
                                        <option value="">-- Selecteer soort melding --</option>
                                        <option value="nieuw">Nieuwe installatie</option>
                                        <option value="storing">Storing</option>
                                        <option value="spoed">Spoed</option>
                                    </select>

                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Notitie') }}</label>
                                <div class="col-md-6">
                                    <textarea type="text" class="form-control @error('note') is-invalid @enderror" name="note">
                                    </textarea>

                                    @error('note')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success float-right">
                                Toevoegen
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
