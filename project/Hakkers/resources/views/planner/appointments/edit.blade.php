@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Afspraak bewerken</div>
                    <div class="card-body">
                        <form action="{{  route('planner.appointments.update', $appointment) }}" method="POST">
                            @csrf
                            {{ method_field('PUT') }}
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Klant') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control @error('customer') is-invalid @enderror" name="customer">
                                        @if ($customers->count())
                                        @foreach($customers as $customer)
                                                <option value="{{ $customer->id }}"
                                                        @if ($customer->id === $appointment->customer_id)
                                                                selected
                                                        @endif
                                                >{{ $customer->name }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    @error('customer')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Monteur') }}</label>
                                <div class="col-md-6">
                                    <select class="form-control @error('monteur') is-invalid @enderror" name="monteur">
                                            @foreach($monteurs as $monteur)
                                                <option value="{{ $monteur->id }}"
                                                        @if ($monteur->id  === $appointment->monteur_id)
                                                        selected
                                                        @endif
                                                >{{ $monteur->name }}</option>
                                            @endforeach
                                    </select>

                                    @error('monteur')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Datum en Tijd') }}</label>
                                <div class="col-md-6">
                                    <input type="datetime-local" id="" class="form-control @error('date') is-invalid @enderror" name="date" value="{{ $date }}T{{$time}}">
                                    @error('date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">{{ __('Notitie') }}</label>
                                <div class="col-md-6">
                                    <textarea type="text" class="form-control @error('note') is-invalid @enderror" name="note">{{ $appointment->note }}
                                    </textarea>

                                    @error('note')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success float-right">
                                Bewerken
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
