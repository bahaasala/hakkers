@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Afspraken</div>

                    <div class="card-body">
                        <a href="{{ route('planner.appointments.create') }}"><button type="button" class="btn btn-success btn-add">Afspraak inplannen</button></a>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Klant</th>
                                <th scope="col">Monteur</th>
                                <th scope="col">Datum en Tijd</th>
                                <th scope="col">Soort melding</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($appointments as $appointment)
                                    <tr>
                                        <td>{{ $appointment->customer->name }}</td>
                                        <td>{{  $appointment->monteur->name }}</td>
                                        <td>{{  $appointment->date }}</td>
                                        <td>{{  $appointment->type }}</td>
                                        <td></td>
                                        <td><i class="fas fa-chevron-right clickable-row" data-href="/planner/appointments/{{$appointment->id}}"></i></td>
                                        <td>
                                            <form action="{{ route('planner.appointments.destroy', $appointment) }}" method="POST" class="float-right">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger delete-warning">Verwijderen</button>
                                            </form>
                                            <a href="{{ route('planner.appointments.edit', $appointment->id) }}"><button type="button" class="btn btn-primary float-right btn-edit">Bewerken</button></a>
                                        </td>
                                    </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
