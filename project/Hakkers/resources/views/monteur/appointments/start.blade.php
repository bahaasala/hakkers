@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Afspraak van {{ $appointment->customer->name }}</div>
                    <div class="row start-actions-container">
                        <form class="col" action="{{route('monteur.departure.appointment', $appointment->id)}}">
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
                            <button type="submit" class="btn btn-primary" onclick="depart()">Vertrekken</button>

                        </form>
                        <form class="col" action="{{route('monteur.arrive.appointment', $appointment->id)}}">
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
                            <button type="submit" class="btn btn-dark" onclick="depart()">Aangekomen</button>

                        </form>
                        <form class="col" action="{{route('monteur.finish.appointment', $appointment->id)}}">
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
                            <button type="submit" class="btn btn-success" onclick="depart()">Klaar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
