@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Afspraken</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Klant</th>
                                <th scope="col">Datum en Tijd</th>
                                <th scope="col">Soort melding</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($appointments))
                            @foreach($appointments as $appointment)
                                @if($appointment->date > $today && $appointment->date < $tomorrow)
                                    @if($appointment->type == 'spoed' && $appointment->successful == '0')
                                        <audio id="play_audio" controls autoplay>
                                            <source src="/audio/alert.mp3" type="audio/mpeg">
                                            Your browser does not support the audio element.
                                        </audio>
                                        <div class="spoed-message">
                                            <p>Er is een spoed afspraak!</p>
                                        </div>
                                    @endif
                                    <tr class="clickable-row
                                            @if($appointment->type == "nieuw")
                                            nieuw
                                            @elseif ($appointment->type == "storing")
                                            storing
                                            @elseif ($appointment->type == "spoed")
                                            spoed
                                            @endif" data-href="/monteur/appointments/{{$appointment->id}}">
                                        <td>{{ $appointment->customer->name }}</td>
                                        <td>{{  $appointment->date }}</td>
                                        <td>{{  $appointment->type }}</td>
                                        <td>@if ($appointment->successful == '1')
                                                <i class="fas fa-check-circle"></i>
                                                @endif
                                        </td>
                                        <td></td>
                                    </tr>
                                @endif
                            @endforeach
                                @else
                                <p>Er zijn geen afspraken ingepland.</p>
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
