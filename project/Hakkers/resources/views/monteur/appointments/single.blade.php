@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header @if($appointment->type == "nieuw")
                            nieuw
                            @elseif ($appointment->type == "storing")
                            storing
                            @elseif ($appointment->type == "spoed")
                            spoed
                            @endif">
                    </div>

                    <div class="card-body">
                        <h2>Afspraak nummer #{{ $appointment->id }}</h2>
                        <p></p>
                        <p><strong>Klant: </strong>{{ $appointment->customer->name }}</p>
                        <p><strong>Monteur: </strong>{{ $appointment->monteur->name }}</p>
                        <p><strong>Adres: </strong>{{ $appointment->customer->address }} {{ $appointment->customer->zipcode }} {{ $appointment->customer->city }}</p>
                        <p><strong>Tel: </strong>{{ $appointment->customer->phone }}</p>
                        <p><strong>Datum en Tijd: </strong>{{ $appointment->date }}</p>
                        <p><strong>Soort melding: </strong>{{ $appointment->type }}</p>
                        <p><strong>Notitie: </strong>{{ $appointment->note }}</p>

                        @if ($appointment->created_at != $appointment->depart_date)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Vertrekt</th>
                                    <th scope="col">Aangekomen</th>
                                    <th scope="col">Klaar</th>
                                    <th>Gewerkte uren</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <td>{{ $appointment->depart_date }}</td>
                                    <td>{{ $appointment->arrived_date }}</td>
                                    <td>{{ $appointment->finished_date }}</td>
                                    <td>{{ $working_hours }}</td>
                                </tbody>
                            </table>
                        @endif

                        @if ($appointment->successful != 1)
                        <a href="{{ route('monteur.start.appointment', $appointment->id) }}"><button type="button" class="btn btn-primary float-left">Start</button></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
