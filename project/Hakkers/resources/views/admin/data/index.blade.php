@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Overzicht afspraken</div>
                    <div class="card-body">
                        <p>Dit is een overzicht van alle afspraken:</p>
                        <p>Klik hieronder op de knop (Download) om het bestand te downloaden</p>
                        <a href="/admin/data/download"><button type="button" class="btn btn-primary">Download</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
