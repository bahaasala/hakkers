@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Monteurs</div>

                    <div class="card-body">
                        <a href="{{ route('admin.monteurs.create') }}"><button type="button" class="btn btn-success btn-add">Monteur toevoegen</button></a>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Naam</th>
                                <th scope="col">Functie</th>
                                <th scope="col">E-mail</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($monteurs as $monteur)
                                    <tr>
                                        <td>{{ $monteur->name }}</td>
                                        <td>{{  $monteur->function }}</td>
                                        <td>{{  $monteur->email }}</td>
                                        <td>
                                            <form action="{{ route('admin.monteurs.destroy', $monteur) }}" method="POST" class="float-right">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger delete-warning">Verwijderen</button>
                                            </form>
                                            <a href="{{ route('admin.monteurs.edit', $monteur->id) }}"><button type="button" class="btn btn-primary float-right btn-edit">Bewerken</button></a>
                                        </td>
                                    </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
