@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Planners</div>

                    <div class="card-body">
                        <a href="{{ route('admin.planners.create') }}"><button type="button" class="btn btn-success btn-add">Planner toevoegen</button></a>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Naam</th>
                                <th scope="col">E-mail</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($planners as $planner)
                                    <tr>
                                        <td>{{ $planner->name }}</td>
                                        <td>{{  $planner->email }}</td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <form action="{{ route('admin.planners.destroy', $planner) }}" method="POST" class="float-right">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger delete-warning">Verwijderen</button>
                                            </form>
                                            <a href="{{ route('admin.planners.edit', $planner->id) }}"><button type="button" class="btn btn-primary float-right btn-edit">Bewerken</button></a>
                                        </td>
                                    </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
